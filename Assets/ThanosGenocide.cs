﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ThanosGenocide : MonoBehaviour {

    [System.Serializable]
    public class KillCountEvent : UnityEvent<int,int>{}
    public KillCountEvent m_onNewKillCount;

    public int m_killed;
    public void Genocide() {

       int  m_oldKilledCount = m_killed;
        foreach (IKillable being in FindObjectsOfType<Killable>())
        {
            bool die = UnityEngine.Random.Range(0, 2)==0;
            if (die)
            {
                being.Kill();
                m_killed++;
            }
        }
        m_onNewKillCount.Invoke(m_oldKilledCount, m_killed);


    }
}
