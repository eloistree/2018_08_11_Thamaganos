﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMoveAround : MonoBehaviour {

    public float m_speed=1;
    public float m_rayDistance = 0.2f;
    public LayerMask m_wallMask;
    void Start () {
		
	}
	

	void Update () {

        bool hasWallInFront = IsThereWallInFront();
        int outLoop = 0;
        while (IsThereWallInFront() || outLoop>50 )
        {
           transform.rotation = GetRandomRotation();
        }

        transform.Translate(Vector3.forward * Time.deltaTime * m_speed);
	}

    private bool IsThereWallInFront()
    {
        return Physics.Raycast(transform.position, transform.forward, m_rayDistance, m_wallMask);
    }

   
    private Quaternion GetRandomRotation()
    {
       return  Quaternion.Euler(UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360));
    }
}
