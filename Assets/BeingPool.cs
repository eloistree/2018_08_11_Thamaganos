﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeingPool : MonoBehaviour {

    public GameObject m_prefab;
    public static BeingPool m_instance;
    public void Start()
    {
        m_instance = this;
    }

    public static void Generate (Vector3 position, Quaternion rotation) {
        GameObject child = GameObject.Instantiate(m_instance.m_prefab, position, rotation);
        Generation gen = child.GetComponent<Generation>();
        child.name = m_instance.m_prefab.name+ ("G "+gen);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
