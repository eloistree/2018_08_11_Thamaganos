﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour {

    public GameObject m_root;
    public float m_duplicationCoolDown=2f;
    public float m_duplicationCurrentCoolDown;


    

    public float m_duplicationRadius=1;
    public void Awake()
    {
        m_duplicationCurrentCoolDown = m_duplicationCoolDown;
    }

    void Update () {

        if (m_duplicationCurrentCoolDown > 0) {

            m_duplicationCurrentCoolDown -= Time.deltaTime;
            if (m_duplicationCurrentCoolDown < 0)
                m_duplicationCurrentCoolDown = 0;
        }

        foreach (Collider hit in Physics.OverlapSphere(transform.position, m_duplicationRadius))
        {
            if (hit.transform != this.transform && hit.GetComponent<LivingBeing>())
            {
                MeetOtherBeing(hit);
            }
        }
     
	}

    private void MeetOtherBeing(Collider collision)
    {
        if (CanDuplicate()) {
            Duplicate();
            ChooseOpposedDirection();
        }
    }

    private void Duplicate()
    {
        m_duplicationCurrentCoolDown = m_duplicationCoolDown;
        BeingPool.Generate( transform.position, GetRandomRotation());
    }

    private void ChooseOpposedDirection()
    {
        transform.forward = -transform.forward;
    }

    public void LockDuplicationFor(float time) {
        m_duplicationCurrentCoolDown = time;
    }

    public bool CanDuplicate() { return m_duplicationCurrentCoolDown <= 0f; }


    private Quaternion GetRandomRotation()
    {
        return Quaternion.Euler(UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360));
    }
}
