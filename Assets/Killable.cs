﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IKillable {
    void Kill();
}


public class Killable : MonoBehaviour, IKillable
{
    public GameObject m_root;

    public void Kill()
    {
        Destroy(m_root);
    }
}
