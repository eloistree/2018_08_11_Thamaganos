﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiveTime : MonoBehaviour {

    public Killable m_killabe;

    public float m_lifeTime = 100;

    public void Update()
    {
        m_lifeTime -= Time.deltaTime;
        if (m_lifeTime < 0)
            m_killabe.Kill();
    }


}
