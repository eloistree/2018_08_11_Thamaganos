﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateKillCountUI : MonoBehaviour {

    public float m_actualCount;
    public float m_currentCount;
    public Text m_dipslay;
	// Use this for initialization
	public void OnNewKillCount (int oldCount, int newCount) {
        m_actualCount = newCount;

    }
	
	// Update is called once per frame
	void Update () {

         m_currentCount = Mathf.Lerp(m_currentCount, m_actualCount, Time.deltaTime);
        m_dipslay.text = "" + (int)m_currentCount;

    }
}
